package com.bsx.springcloud.service;

import org.springframework.stereotype.Component;

/**
 * 服务降级统一异常兜底处理
 * 实现 OpenFeign 的service接口
 */
@Component
public class PaymentFallbackService implements PaymentHystrixService{
    @Override
    public String paymentInfo_OK(Integer id) {
        return "--- PaymentFallbackService paymentInfo_OK 方法 服务降级： /(ToT)/ ";
    }

    @Override
    public String paymentInfo_TimeOut(Integer id) {
        return "--- PaymentFallbackService paymentInfo_TimeOut 方法 服务降级： /(ToT)/ ";
    }
}
