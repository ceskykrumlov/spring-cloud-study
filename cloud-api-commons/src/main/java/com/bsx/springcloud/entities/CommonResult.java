package com.bsx.springcloud.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 统一响应
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CommonResult<T> {
    private Integer code; // 状态码
    private String message; // 信息
    private T data; // 数据实体

    public CommonResult(Integer code, String message) {
        // 空数据的构造方法
        this(code, message, null);
    }
}
