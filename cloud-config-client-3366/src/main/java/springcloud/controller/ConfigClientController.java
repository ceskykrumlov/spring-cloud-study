package springcloud.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RefreshScope
public class ConfigClientController {
    @Value("${config.info}") // 与远程 git 仓库上 yml 文件中的属性一致，就读取这个配置属性
    private String configInfo;

    @GetMapping("/configInfo")
    public String getConfigInfo() {
        return "3366: " + configInfo;
    }
}
