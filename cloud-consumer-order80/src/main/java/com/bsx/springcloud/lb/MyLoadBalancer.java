package com.bsx.springcloud.lb;

import org.springframework.cloud.client.ServiceInstance;

import java.util.List;

/**
 * 自定义负载均衡器接口
 * - 获取 注册中心 所有活着的微服务节点
 */
public interface MyLoadBalancer {

    /**
     * 给我一个 微服务实例 的列表，我根据负载均衡策略，选一个具体的微服务实例，访问它
     *
     * @param serviceInstances 注册中心上所有可用微服务实例的列表
     * @return 最后选择的要访问的那个实例
     */
    ServiceInstance instance(List<ServiceInstance> serviceInstances);
}
