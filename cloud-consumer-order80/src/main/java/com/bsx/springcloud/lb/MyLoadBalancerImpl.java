package com.bsx.springcloud.lb;

import org.springframework.cloud.client.ServiceInstance;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

@Component
public class MyLoadBalancerImpl implements MyLoadBalancer {
    private AtomicInteger atomicInteger = new AtomicInteger(0);

    @Override
    public ServiceInstance instance(List<ServiceInstance> serviceInstances) {
        int index = getAndIncrement() % serviceInstances.size(); // 获取下标
        return serviceInstances.get(index);
    }

    // 原子加1
    private final int getAndIncrement() {
        int current;
        int next;
        do {
            current = this.atomicInteger.get(); // 获取原子整型的值
            // 如果current超出最大值，就变为0，否则+1
            next = current >= Integer.MAX_VALUE ? 0 : current + 1;
        } while (!atomicInteger.compareAndSet(current, next));
        System.out.println("****** next: " + next + "******");
        return next;
    }
}
