package com.bsx.springcloud.controller;

import com.bsx.springcloud.entities.CommonResult;
import com.bsx.springcloud.entities.Payment;
import com.bsx.springcloud.lb.MyLoadBalancer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.net.URI;
import java.util.List;

@RestController
@Slf4j
public class OrderController {
    //    private static final String PAYMENT_URL = "http://localhost:8081";
    // 基于 Eureka Server Application 服务名称
    private static final String PAYMENT_URL = "http://CLOUD-PAYMENT-SERVICE";

    @Resource
    private RestTemplate restTemplate;
    // 注入自定义的负载均衡器
    @Resource
    private MyLoadBalancer myLoadBalancer; // aka impl: MyLoadBalancerImpl
    // discoveryClient 用来扫 Eureka Server 上的实例列表
    @Resource
    private DiscoveryClient discoveryClient;

    /**
     * 调用 支付模块 的 create 方法
     *
     * @param payment
     * @return 调用结果
     */
    @GetMapping("/consumer/payment/create")
    public CommonResult<Integer> create(Payment payment) {
        return restTemplate.postForObject(PAYMENT_URL + "/payment/create",
                payment,
                CommonResult.class);
    }

    @GetMapping("/consumer/payment/get/{id}")
    public CommonResult<Payment> getPayment(@PathVariable("id") Long id) {
        return restTemplate.getForObject(PAYMENT_URL + "/payment/get/" + id,
                CommonResult.class);
    }

    @GetMapping("/consumer/payment/getForEntity/{id}")
    public CommonResult<Payment> getPayment2(@PathVariable("id") Long id) {
        ResponseEntity<CommonResult> entity = restTemplate.getForEntity(PAYMENT_URL + "/payment/get/" + id, CommonResult.class);
        if (entity.getStatusCode().is2xxSuccessful()) // 如果是 200 系列响应码
            return entity.getBody(); // 返回请求体
        else
            return new CommonResult<>(444, "获取失败");
    }

    /**
     * 使用自定义负载均衡
     */
    @GetMapping("/consumer/payment/lb")
    public String getPaymentLB() {
        List<ServiceInstance> instances = discoveryClient.getInstances("CLOUD-PAYMENT-SERVICE");
        if (instances == null || instances.size() <= 0) return null;
        // 有效服务列表
        // 使用自定义的负载均衡策略，从列表里获取一个目标微服务实例
        ServiceInstance targetInstance = myLoadBalancer.instance(instances);
        URI uri = targetInstance.getUri();

        return restTemplate.getForObject(uri + "/payment/lb", String.class);
    }

    @GetMapping(value = "/consumer/payment/zipkin")
    public String paymentZipkin() {
        return restTemplate.getForObject("http://localhost:8081/payment/zipkin/", String.class);
    }
}
