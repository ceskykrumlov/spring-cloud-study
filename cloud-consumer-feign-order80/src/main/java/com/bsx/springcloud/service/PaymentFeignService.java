package com.bsx.springcloud.service;

import com.bsx.springcloud.entities.CommonResult;
import com.bsx.springcloud.entities.Payment;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(value = "CLOUD-PAYMENT-SERVICE")
public interface PaymentFeignService {

    @GetMapping(value = "/payment/get/{id}")
    CommonResult<Payment> getPaymentById(@PathVariable("id") Long id);

    // 调 payment 的超时端口
    @GetMapping("/payment/feign/timeout")
    String paymentFeignTimeout();
}