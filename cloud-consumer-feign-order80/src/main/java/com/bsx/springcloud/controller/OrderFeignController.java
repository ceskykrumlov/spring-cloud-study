package com.bsx.springcloud.controller;

import com.bsx.springcloud.entities.CommonResult;
import com.bsx.springcloud.entities.Payment;
import com.bsx.springcloud.service.PaymentFeignService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@Slf4j
public class OrderFeignController {

    @Resource
    private PaymentFeignService paymentFeignService;

    @GetMapping("/consumer/payment/get/{id}")
    public CommonResult<Payment> getPaymentById(@PathVariable("id") Long id) {
        // 通过 openFeign 去 Eureka 找微服务，调那个微服务的 Controller 接口
        return paymentFeignService.getPaymentById(id);
    }
    // openFeign 转成调用 provider 上的超时方法（Controller）
    @GetMapping("/consumer/payment/feign/timeout")
    public String paymentFeignTimeOut() {
        return paymentFeignService.paymentFeignTimeout();
    }
}
