package com.bsx.springcloud.controller;

import com.bsx.springcloud.entities.CommonResult;
import com.bsx.springcloud.entities.Payment;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.*;
import com.bsx.springcloud.service.PaymentService;

import javax.annotation.Resource;
import java.util.List;

@Slf4j
@RestController
public class PaymentController {

    @Resource
    private PaymentService paymentService;

    @Value("${server.port}") // 端口号
    private String serverPort; // 从application.yaml中读取

    @Resource // 服务发现 Discovery
    private DiscoveryClient discoveryClient;

    @GetMapping("/payment/discovery")
    public Object discovery() {
        // 获取 Eureka 注册中心中的 服务名称列表
        List<String> services = discoveryClient.getServices();
        for (String service : services) {
            log.info("********* service: [" + service + "] *********");

        }
        // 通过服务名获取其对应的所有服务节点
        List<ServiceInstance> instances = discoveryClient.getInstances("CLOUD-PAYMENT-SERVICE");
        for (ServiceInstance instance : instances) {
            // id 主机名 端口号 URI
            log.info("********* " + instance.getInstanceId() +
                    "\t" + instance.getHost() +
                    "\t " + instance.getPort() +
                    "\t" + instance.getUri() + " *********");
        }
        // 返回当前的 discoveryClient 对象
        return this.discoveryClient;
    }


    @PostMapping("/payment/create")
    public CommonResult<Integer> create(@RequestBody Payment payment) {
        int result = paymentService.create(payment);
        log.info("=====插入结果：" + result);

        return result > 0 ? new CommonResult<>(200, "插入成功，端口号: " + serverPort, result)
                : new CommonResult<>(444, "插入失败", null);
    }

    @GetMapping("/payment/get/{id}")
    public CommonResult<Payment> getPaymentById(@PathVariable("id") Long id) {
        Payment payment = paymentService.getPaymentById(id);
        return payment != null ? new CommonResult<>(200, "查询成功，端口号：" + serverPort, payment) :
                new CommonResult<>(444, "查询失败", null);
    }


    @GetMapping("/payment/lb")
    public String getPaymentLB() {
        return serverPort;
    }
}
