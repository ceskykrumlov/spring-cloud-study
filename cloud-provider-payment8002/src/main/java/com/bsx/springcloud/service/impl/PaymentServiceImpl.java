package com.bsx.springcloud.service.impl;

import com.bsx.springcloud.dao.PaymentDao;
import com.bsx.springcloud.entities.Payment;
import org.springframework.stereotype.Service;
import com.bsx.springcloud.service.PaymentService;

import javax.annotation.Resource;

@Service
public class PaymentServiceImpl implements PaymentService {
    @Resource
    private PaymentDao paymentDao;

    @Override
    public int create(Payment payment) {
        return paymentDao.create(payment);
    }

    @Override
    public Payment getPaymentById(Long id) {
        return paymentDao.getPaymentById(id);
    }
}
