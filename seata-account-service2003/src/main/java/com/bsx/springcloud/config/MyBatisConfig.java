package com.bsx.springcloud.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@MapperScan({"com.bsx.springcloud.dao"})
public class MyBatisConfig {

}