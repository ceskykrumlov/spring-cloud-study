package com.bsx.springcloud.service;

import com.bsx.springcloud.entities.Payment;
import org.apache.ibatis.annotations.Param;

public interface PaymentService {
    int create(Payment payment); // 添加一条payment记录

    Payment getPaymentById(@Param("id") Long id); // 根据id查询payment
}
