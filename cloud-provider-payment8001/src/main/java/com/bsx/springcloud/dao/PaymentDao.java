package com.bsx.springcloud.dao;

import com.bsx.springcloud.entities.Payment;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface PaymentDao {
    int create(Payment payment); // 添加一条payment记录

    Payment getPaymentById(@Param("id") Long id); // 根据id查询payment
}
