package com.bsx.springcloud.service;

import com.bsx.springcloud.domain.Order;

public interface OrderService {

    /**
     * 创建订单
     * @param order
     */
    void create(Order order);
}