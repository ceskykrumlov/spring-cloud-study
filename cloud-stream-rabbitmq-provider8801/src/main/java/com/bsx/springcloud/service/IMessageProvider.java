package com.bsx.springcloud.service;

public interface IMessageProvider {
    /**
     * 发送消息
     */
    String send();
}
