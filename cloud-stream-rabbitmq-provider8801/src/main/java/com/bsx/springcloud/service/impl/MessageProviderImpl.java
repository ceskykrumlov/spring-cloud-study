package com.bsx.springcloud.service.impl;

import com.bsx.springcloud.service.IMessageProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.MessageChannel;

import java.util.UUID;


/**
 * 与 RabbitMQ 交互的 Service 实现类
 * 基于 Stream Binder 进行交互
 */
@EnableBinding(Source.class) // 引入 cloud.stream 的 Source，表示生产者
public class MessageProviderImpl implements IMessageProvider {

    @Autowired
    // @Qualifier("nullChannel")
    private MessageChannel output; // 消息发送管道

    @Override
    public String send() {
        String serialNum = UUID.randomUUID().toString();
        output.send(MessageBuilder.withPayload(serialNum).build());
        System.out.println("serialNum = " + serialNum);
        return serialNum;
    }
}
