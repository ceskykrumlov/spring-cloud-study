package com.bsx.springcloud.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient("nacos-payment-provider")
public interface FeignService {
    @GetMapping("payment/nacos/{id}")
    String getPaymentById(@PathVariable("id") Integer id);
}
