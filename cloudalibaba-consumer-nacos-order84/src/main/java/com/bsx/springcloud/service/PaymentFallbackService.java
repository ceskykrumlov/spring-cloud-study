package com.bsx.springcloud.service;

import com.bsx.springcloud.entities.CommonResult;
import com.bsx.springcloud.entities.Payment;
import org.springframework.stereotype.Component;

@Component
public class PaymentFallbackService implements PaymentFeignService {

    @Override // 指定 paymentSQL 方法失败后的处理代码
    public CommonResult<Payment> paymentSQL(Long id) {
        return new CommonResult<>(444,
                "服务降级返回，---PaymentFallbackService",
                new Payment(id, "ErrorSerial"));
    }
}